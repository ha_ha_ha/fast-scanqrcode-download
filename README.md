快捷扫码下载（Fast-ScanQRCode-Download）就是这么直接的翻译

纯绿色，开源..
依赖java环境、局域网


### 这玩意是干啥的？
> 局域网内 电脑->手机 文件网络传输工具
> 简单来说： 局域网内，在电脑上右键选择一个文件，即可弹出一个二维码。手机扫码即可下载该文件

### 为什么要写这个
* 电脑传文件到手机上，百度搜索出来的工具，操作太麻烦，真是烦死了...没找到方便的工具，自己做一个吧。。
* 微信传文件占用手机内存，而且微信会改后缀


### 如何安装到电脑

第一种： clone 源码，自己编译 `mvn clean package`

第二种： 自行到release页面下载zip包，解压即可

### 使用方法

第一种：命令行模式
```
bin/Fast-ScanQRCode-Download.bat "文件全路径"
```

*[推荐]* 第二种： 右键菜单方式（需要添加右键菜单，windows下我写了一个简单的批处理脚本），管理员权限执行install.bat即可
> 全开源，你也不用担心啥后门


### 操作步骤...
1、windows下右键菜单生成二维码
2、手机端扫码下载

觉得可以就Star，欢迎优化和扩展实用

